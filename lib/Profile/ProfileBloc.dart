import 'ProfileModel.dart';


class ProfileBloc {
  List<Profile> results = [];

  ProfileBloc.fromJson(Map<String, dynamic> response){
    List<Profile> temp = [];
    for(int i = 0; i < response['results'].length; i++) {
      Profile data = Profile(response['results'][i]);
      temp.add(data);
    }
    results = temp;
  }
}