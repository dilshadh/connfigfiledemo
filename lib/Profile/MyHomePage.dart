import 'package:flutter/material.dart';
import 'package:blocrx_demo/Common/APIClient.dart';
import 'package:blocrx_demo/Dashboard/Dashboard.dart';
import 'package:blocrx_demo/Profile/ProfileBloc.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  buildUI(BuildContext context, ProfileBloc client) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        Container(
            width: 190.0,
            height: 190.0,
            decoration: new BoxDecoration(
                shape: BoxShape.circle,
                image: new DecorationImage(
                    fit: BoxFit.fill,
                    image: new NetworkImage(client.results.first.img.large)))),
        SizedBox(
          height: 15,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
                'Name  : ${client.results.first.name.first} ${client.results
                    .first.name.last}', style: Theme.of(context).textTheme.title),
            Text('Gender: ${client.results.first.gender}'),
            Text('Date of Birth: ${client.results.first.dob}'),
            Text('Address: ${client.results.first.address.street}, '
                '${client.results.first.address.city}, '
                '\n ${client.results.first.address.state}'),
            SizedBox(
              height: 30,
            ),

               ButtonTheme(
//                buttonColor: Colors.red,
                minWidth: 100,
                height: 50,
                hoverColor: Colors.yellowAccent,
                child: SizedBox(
                  height: 50,
                  width: 150,
                  child: RaisedButton(
                    
                    color: Colors.green,
                    onPressed: () {
                      Navigator.push(
                          context, new MaterialPageRoute(
                          builder: (context) => Dashboard(client.results.first)));
                    },
                    child: Center(
                      child: Text("Enter", style: Theme.of(context).textTheme.button ,),
                    ),
                  ),
                ),
              ),

          ],
        ),
      ],
    );
  }

  SimpleDialog showLoader() {
    return SimpleDialog(
      elevation: 300.0,
      backgroundColor: Colors.black45,

      children: <Widget>[
        Center(
          child: CircularProgressIndicator(),
        ),
        Center(
          child: Text("Loading..."),
        )

      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    APIClient client = APIClient();
    Future<ProfileBloc> data = client.fetchPost();

    return new Scaffold(
      appBar: new AppBar(title: Text("Profile"),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            StreamBuilder(
                stream: client.observableProfileData,
                builder: (context, AsyncSnapshot<ProfileBloc> snapshot) {
                  if (snapshot.hasData) {
                    return buildUI(context, snapshot.data);
                  }
                  return showLoader();
                })
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
