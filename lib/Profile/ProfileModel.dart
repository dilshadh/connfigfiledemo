import 'package:intl/intl.dart';


class Profile {
  String gender;
  Name name;
  String email;
  ImageUrl img;
  String dob;
  Address address;

  Profile(data){
    gender = data['gender'];
    name = Name(data['name']);
    img = ImageUrl(data['picture']);
    email = data['email'];
    address = Address(data["location"]);
    var dateInString = data["dob"]["date"];
    DateTime date = DateTime.parse(dateInString);
    var formatter = new DateFormat('dd MMM yyyy');
    dob = formatter.format(date);
  }
}

class Name {
  String title;
  String first;
  String last;

  Name(data){
    title = data['title'];
    first = data['first'];
    last = data['last'];
  }
}

class ImageUrl {

  String small;
  String large;
  String thumbnail;

  ImageUrl(data){
    small = data['medium'];
    large = data['large'];
    thumbnail = data['thumbnail'];
    print("thumbnail  {$thumbnail}");
  }
}


class Address {

  String street;
  String city;
  String state;
//  String postcode;
  
  Address(json){
    street = json['street'];
    city = json['city'];
    state = json['state'];
    
    
  }
  



}
