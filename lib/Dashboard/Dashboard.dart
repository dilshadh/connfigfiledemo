import 'package:blocrx_demo/Common/app_card.dart';
import 'package:flutter/material.dart';
import 'package:blocrx_demo/Profile/ProfileModel.dart';
import 'package:blocrx_demo/Common/APIClient.dart';
import 'package:blocrx_demo/Dashboard/UserBloc.dart';


class Dashboard extends StatefulWidget {

  Profile profileInfo;
  Dashboard(this.profileInfo) : super();
  @override
  _DashboardState createState() => new _DashboardState();

}


class _DashboardState extends State<Dashboard> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.profileInfo);
    print('widget %%%%%%%%%%%% ${widget.profileInfo}');
  }

  buildUI(BuildContext context, Userbloc client) {
    return ListView.builder(
        itemCount: client.results.length,
        itemBuilder: (context, index) {
          return AppCard(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.account_circle, color: Colors.green,),
                SizedBox(width: 20,),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                      Text("Name: ${client.results[index].name}", style: Theme.of(context).textTheme.body1 ),
                      Text("Email: ${client.results[index].email}",style: TextStyle(color: Colors.red, fontWeight: FontWeight.normal)),

                  ],
                ),

              ],
            ),
          );
        }
    );
  }

  SimpleDialog showLoader() {

    return SimpleDialog(

      elevation: 300.0,
      backgroundColor: Colors.yellowAccent,

      children: <Widget>[
        Center(
          child: CircularProgressIndicator(),
        ),
        Center(
          child: Text("Loading..."),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    APIClient client = APIClient();
    Future<Userbloc> data = client.fetchUserData();

    return new Scaffold(

      appBar: new AppBar(title: Text("Dashboard")),
      body:
            StreamBuilder(
                stream: client.users,
                builder: (context, AsyncSnapshot<Userbloc> snapshot) {
                    if (snapshot.hasData) {
                     return  buildUI(context, snapshot.data);
                    } else {
                      return showLoader();
                    }
                }
                ),


    );
  }


}