import 'package:flutter/material.dart';
import 'package:blocrx_demo/Profile/MyHomePage.dart';
import 'package:blocrx_demo/Common/Config.dart';

void main() => runApp(AppConfigFile());

class AppConfigFile extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        appBarTheme: Config.appBarTheme,
        scaffoldBackgroundColor: Config.scaffoldBackgroundColor,
        primarySwatch: Config.primarySwatch,
        // text theme of app
        textTheme: Config.textTheme,

      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

