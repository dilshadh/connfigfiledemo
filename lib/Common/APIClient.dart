import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'package:blocrx_demo/Dashboard/UserBloc.dart';
import 'dart:convert';
import 'package:rxdart/rxdart.dart';
import 'package:blocrx_demo/Profile/ProfileBloc.dart';

class APIClient {

  //******Profile Screen
  final dataFetcher = PublishSubject<ProfileBloc>();
  Observable<ProfileBloc> get observableProfileData => dataFetcher.stream;

  Future<ProfileBloc> fetchPost() async{
    print("Will hit API");
    var response = await http.get('https://randomuser.me/api/');
    var jsonData  = response.body.toString();
    ProfileBloc data = ProfileBloc.fromJson(json.decode(jsonData));
    dataFetcher.sink.add(data);
    print(data);
  }


  //******Dashboard
  final usersObservable = PublishSubject<Userbloc>();
  Observable<Userbloc> get users => usersObservable.stream;

  Future<Userbloc> fetchUserData() async{
    print("Will hit API");
    var response = await http.get('https://jsonplaceholder.typicode.com/users');
    var jsonData  = response.body.toString();
    Userbloc data = Userbloc.json(json.decode(response.body) as List);
    usersObservable.sink.add(data);
    print("###########: ${data.results.first.email}");
    print(data.results.first.email);
  }

}



