import 'package:flutter/material.dart';

class Config {


  //*** Text/Button text themes
  static TextStyle _body1TextStyle = TextStyle(color: Colors.blueAccent, fontWeight: FontWeight.w500);
  static TextStyle _buttoTextStyle = TextStyle(color: Colors.yellowAccent);
  static TextStyle _body2TextStyle = TextStyle(color: Colors.yellowAccent);
  static TextTheme textTheme = TextTheme(title: TextStyle(color: Colors.black, fontStyle: FontStyle.normal),
      body1: _body1TextStyle,
      button: _buttoTextStyle,
      body2: _body2TextStyle);

  //*** Appbar Style
  static TextStyle _textStyle = TextStyle(color: Colors.green, fontWeight: FontWeight.bold, fontSize: 15.0);
  static TextTheme _appBarTextTheme =  TextTheme(title: _textStyle);

  static AppBarTheme appBarTheme = AppBarTheme(color: Colors.red,
    textTheme: _appBarTextTheme,
    brightness: Brightness.light,
  );

  static Color scaffoldBackgroundColor = Colors.lime;
  static MaterialColor primarySwatch = Colors.blue;

}