import 'package:flutter/material.dart';


class AppCard extends StatefulWidget {
  final Widget child;
  AppCard({Key key, @required this.child}) : super(key: key);


  @override
  _AppCardState createState() => _AppCardState();
}


class _AppCardState extends State<AppCard> {
  @override
  Widget build(BuildContext context) {

    return Column(
//      mainAxisSize: MainAxisSize.min,
//      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Card(
          margin: EdgeInsets.all(10.0),
          color: Colors.white,
          child: Container(
            padding: EdgeInsets.all(10.0),
            child: widget.child,

          ),
        )

        ],
    );
  }
}



class AppConfig extends StatefulWidget {
  @override
  _AppConfigState createState() => _AppConfigState();
}

class _AppConfigState extends State<AppConfig> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return null;
  }
}
